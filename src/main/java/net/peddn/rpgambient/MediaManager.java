/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.peddn.rpgambient;

import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author peddn
 */
public class MediaManager extends Stage {
    
    public MediaManager() {
        
        this.setTitle("Media Manager");
        
        BorderPane border = new BorderPane();
        
        Scene scene = new Scene(border, 1024, 768);
        
        this.setScene(scene);
        
        this.initModality(Modality.WINDOW_MODAL);
        this.initOwner(RPGAmbient.stage);
        this.setOnHidden(e -> System.out.println("Window-level cleanup..."));

    }
    
}
