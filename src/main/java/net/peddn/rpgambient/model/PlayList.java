package net.peddn.rpgambient.model;

import java.util.ArrayList;
import java.util.List;

public class PlayList {
    
    private String name;
    
    private boolean loop;
    
    private final List<Sound> sounds;
    
    // der aktuell ausgewaehlte Sound
    private Sound actSound;
    
    public PlayList(String name) {
        this.name = name;
        this.loop = false;
        this.sounds = new ArrayList();
        this.actSound = null;
    }
    
    public void addSound(Sound sound) {

        sound.getPlayer().setOnReady(() -> {
            System.out.println(sound.getName() + " ready");
        });
        
        sound.getPlayer().setOnPlaying(() -> {
            System.out.println(sound.getName() + " playing");
        });
        sound.getPlayer().setOnStalled(() -> {
            System.out.println(sound.getName() + " stalled");
        });
        sound.getPlayer().setOnStopped(() -> {
            System.out.println(sound.getName() + " stopped");
        });
        sound.getPlayer().setOnEndOfMedia(()-> {
            this.stopAll();
            System.out.println(sound.getName() + " eof");
            int index = this.sounds.indexOf(sound);
            if(index == -1) {
                System.out.println("Sound not in this paylist");
            } else {
                // wenn es der letzte Track in der PlayList ist
                if(index == this.sounds.size() - 1) {
                    this.reset();
                    if(isLoop() && (this.actSound != null)) {
                        this.actSound.getPlayer().play();
                    }
                } else {
                    this.actSound = this.sounds.get(index+1);
                    this.actSound.getPlayer().play();
                }
                
            }
        });
        
        this.sounds.add(sound);
        
    }
    
    private void reset() {
        if(this.sounds.size() > 0) {
            this.actSound = this.sounds.get(0);
            for(Sound sound : this.sounds) {
                sound.getPlayer().stop();
                sound.getPlayer().seek(sound.getPlayer().getStartTime());
            }
        } else {
            this.actSound = null;
        }
        
    }
    
    
    
    private void stopAll() {
        for(Sound sound : this.sounds) {
            sound.getPlayer().stop();
            sound.getPlayer().seek(sound.getPlayer().getStartTime());
        }
    }
    

    
    public void addSoundAtIndex(int index, Sound sound) {
        this.sounds.add(index, sound);
    }
    
    public void play() {
        this.reset();
        if(this.sounds.size() > 0) {
            Sound sound = this.sounds.get(0);
            sound.getPlayer().play();
        } else {
            System.out.println("cannot play empty playlist");
        }
    }
    
    public void playTrack(int track) {
        this.stopAll();
        this.actSound = this.sounds.get(track);
        this.actSound.getPlayer().play();
    }
    
    public void pause() {
        this.actSound.getPlayer().pause();
    }
    
    public void resume() {
        this.actSound.getPlayer().play();
    }
    
    public void stop() {
        this.stopAll();
    }
    
    
    
    public void setVolume() {
        
    }
    
    public void mute() {
        for(Sound sound : sounds) {
            sound.getPlayer().setMute(true);
        }
    }
    
    public void unmute() {
        for(Sound sound : sounds) {
            sound.getPlayer().setMute(false);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isLoop() {
        return loop;
    }
    
    public void setLoop(boolean loop) {
        this.loop = loop;
    }

}
