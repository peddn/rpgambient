package net.peddn.rpgambient.model;

import java.io.File;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;


public class Sound {
    
    private MediaPlayer player;
    private String name;
    private String path;
    
    public Sound(String name, String path) {
    
        this.name = name;
        this.path = path;
        
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(path).getFile());
        Media sound = new Media(file.toURI().toString());
        
        this.player = new MediaPlayer(sound);

    }
    
    public void reset() {
        this.player.seek(this.player.getStartTime());
    }
    
    public MediaPlayer getPlayer() {
        return this.player;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
    
    
    
}
