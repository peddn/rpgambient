/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.peddn.rpgambient.model;

/**
 *
 * @author peddn
 */
public class Scenery {
    
    private String name;
    private String description;
    
    public Scenery(String name) {
        this.name = name;
        this.description = "";
    }
    
    @Override
    public String toString() {
        return this.name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
