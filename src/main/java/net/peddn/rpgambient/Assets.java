/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.peddn.rpgambient;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author peddn
 */
public final class Assets {

    private static HashMap<String, ImageView> icons = new HashMap();

    private static ClassLoader cl = Assets.class.getClassLoader();

    private Assets() {

    }

    public static void loadIcons() {

        File iconsPath = new File(Assets.cl.getResource("ico").getFile());

        File[] iconFiles = iconsPath.listFiles();

        List<String> iconPaths = new ArrayList();

        for (File iconFile : iconFiles) {
            if(iconFile.isFile()) {
                iconPaths.add(iconFile.getName());
            }
        }

        for(String iconName : iconPaths) {
            if (iconName.endsWith(".png")) {
                Image iconImage = new Image(Assets.cl.getResourceAsStream("ico/" + iconName));
                ImageView iconView = new ImageView(iconImage);
                iconName = iconName.substring(0, iconName.length() - 4);
                Assets.icons.put(iconName, iconView);
                System.out.println("loaded icon '" + iconName + "'");
            }

        }

    }
    
    public static ImageView getIcon(String iconName) {
        return Assets.icons.get(iconName);
    }

}
